# RevoId

**Elixir project to analyze Revolut id verification challenge**

## To Run

Install elixir:
https://elixir-lang.org/install.html

After install, verify version > 1.7:
`$elixir --version`
```
Erlang/OTP 21 [erts-10.1.1] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [hipe] [dtrace]

Elixir 1.7.4 (compiled with Erlang/OTP 21)
```
cd into revo_id directory.

install deps with:
`$mix deps.get`

run tests with:
`$mix test`

run project with iex:
`$iex -S mix`

with project running run analsis script if desired:
`iex> c "analysis.exs"`

or make calls to server:
`iex> users = RevoId.DataServer.get_all(:users)`

or:
`iex> users_passed = RevoId.DataServer.get_where(:users, :overal_result, :clear)`
