alias RevoId.DataServer, as: Server
alias RevoId.GetHelpers, as: Get

# attempts
attempts = Server.get_all(:attempts)
total_attempts = attempts |> Enum.count()

IO.puts("total non-duplicate attempts: #{total_attempts}")

clear_attempts =
  attempts
  |> Get.get_where(:overall_result, :clear)

total_clear_attempts = clear_attempts |> Enum.count()

IO.puts("Cleared attempts: #{total_clear_attempts}")

prob_of_clear_attempt = Float.round(total_clear_attempts / total_attempts, 4)

IO.puts("% of attempts cleared: #{prob_of_clear_attempt * 100}%\n")

# users
users = Server.get_all(:users)
total_users = users |> Enum.count()

IO.puts("total users: #{total_users}")

clear_users =
  users
  |> Get.get_where(:overall_result, :clear)

total_clear_users = clear_users |> Enum.count()

IO.puts("Users cleared in last attempt: #{total_clear_users}")

prob_of_clear_user = Float.round(total_clear_users / total_users, 4)

IO.puts("% of users cleared in last attempt: #{prob_of_clear_user * 100}%\n")

users_with_one_attempt = users |> Get.get_where(:num_attempts, 1)

total_users_with_one_attempt = users_with_one_attempt |> Enum.count()

IO.puts("Total users with only one attempt: #{total_users_with_one_attempt}")

clear_users_with_one_attempt = users_with_one_attempt |> Get.get_where(:overall_result, :clear)

consider_users_with_one_attempt =
  users_with_one_attempt |> Get.get_where(:overall_result, :consider)

total_clear_users_with_one_attempt = clear_users_with_one_attempt |> Enum.count()
total_consider_users_with_one_attempt = consider_users_with_one_attempt |> Enum.count()

IO.puts("Total users with only one attempt that cleared: #{total_clear_users_with_one_attempt}")

IO.puts(
  "Total users with only one attempt that did not clear: #{total_consider_users_with_one_attempt}"
)

users_that_cleared_on_first_attempt =
  users
  |> Get.get_users_where_attempt_result(1, :clear, :clear)

total_users_that_cleared_on_first_attempt = users_that_cleared_on_first_attempt |> Enum.count()

IO.puts("Total users that cleared on first try: #{total_users_that_cleared_on_first_attempt}")

IO.puts(
  "Total users that cleared on first try but for some reason tried again: #{
    total_users_that_cleared_on_first_attempt - total_clear_users_with_one_attempt
  }"
)

users_doc_caution_fac_clear_first_try =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :clear)

users_doc_rejected_fac_clear_first_try =
  users
  |> Get.get_users_where_attempt_result(1, :rejected, :clear)

users_doc_suspected_fac_clear_first_try =
  users
  |> Get.get_users_where_attempt_result(1, :suspected, :clear)

users_doc_clear_fac_consider_first_try =
  users
  |> Get.get_users_where_attempt_result(1, :clear, :consider)

users_doc_rejected_fac_consider_first_try =
  users
  |> Get.get_users_where_attempt_result(1, :rejected, :consider)

users_doc_suspected_fac_consider_first_try =
  users
  |> Get.get_users_where_attempt_result(1, :suspected, :consider)

users_doc_caution_fac_consider_first_try =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :consider)

IO.puts("""
First_try count:
doc_result | fac_result |  overall  | count
clear          clear        clear       #{total_users_that_cleared_on_first_attempt}
caution        clear        consider    #{users_doc_caution_fac_clear_first_try |> Enum.count()}
suspected      clear        consider   #{users_doc_suspected_fac_clear_first_try |> Enum.count()}
rejected       clear         consider   #{users_doc_rejected_fac_clear_first_try |> Enum.count()}
clear          consider      consider    #{users_doc_clear_fac_consider_first_try |> Enum.count()}
caution        consider      consider    #{
  users_doc_caution_fac_consider_first_try |> Enum.count()
}
suspected      consider      consider    #{
  users_doc_suspected_fac_consider_first_try |> Enum.count()
}
rejected       consider      consider    #{
  users_doc_rejected_fac_consider_first_try |> Enum.count()
}
""")

total_doc_clear_fac_consider_first_try = users_doc_clear_fac_consider_first_try |> Enum.count()

total_doc_clear_first_try =
  total_doc_clear_fac_consider_first_try + total_users_that_cleared_on_first_attempt

IO.puts("Total doc_clear on first try: #{total_doc_clear_first_try}")

fail_map_doc_caution_fac_clear_first_try =
  users_doc_caution_fac_clear_first_try |> Get.get_consider_count_per_result_attempt_num(0)

IO.puts("fail map doc caution fac clear")
IO.inspect(fail_map_doc_caution_fac_clear_first_try)

fail_map_doc_caution_fac_consider_first_try =
  users_doc_caution_fac_consider_first_try |> Get.get_consider_count_per_result_attempt_num(0)

IO.puts("fail map doc caution fac consider")
IO.inspect(fail_map_doc_caution_fac_consider_first_try)

fail_map_doc_rejected_fac_clear_first_try =
  users_doc_rejected_fac_clear_first_try |> Get.get_consider_count_per_result_attempt_num(0)

IO.puts("fail map doc rejected fac clear")
IO.inspect(fail_map_doc_rejected_fac_clear_first_try)

fail_map_doc_rejected_fac_consider_first_try =
  users_doc_rejected_fac_consider_first_try |> Get.get_consider_count_per_result_attempt_num(0)

IO.puts("fail map doc rejected fac clear")
IO.inspect(fail_map_doc_rejected_fac_consider_first_try)

fail_map_doc_clear_fac_consider_first_try =
  users_doc_clear_fac_consider_first_try |> Get.get_consider_count_per_result_attempt_num(0)

IO.puts("fail map doc clear fac consider")
IO.inspect(fail_map_doc_clear_fac_consider_first_try)

users_that_caution_on_first_attempt_but_clear_the_second =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :clear)
  |> Get.get_where(:overall_result, :clear)

total_users_that_caution_on_first_attempt_with_two_tries =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :clear)
  |> Get.get_where(:num_attempts, 2)
  |> Enum.count()

IO.puts("caution first two tries: #{total_users_that_caution_on_first_attempt_with_two_tries}")

total_users_that_caution_on_first_attempt_but_clear_the_second =
  users_that_caution_on_first_attempt_but_clear_the_second |> Enum.count()

IO.puts(
  "caution first clear second: #{total_users_that_caution_on_first_attempt_but_clear_the_second}"
)

users_that_caution_on_first_attempt_but_caution_the_second_fac_clear =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :clear)
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_users_where_attempt_result(2, :caution, :clear)

users_that_caution_on_first_attempt_but_caution_the_second_fac_consider =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :clear)
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_users_where_attempt_result(2, :caution, :consider)

total_users_that_caution_on_first_attempt_but_caution_the_second_fac_clear =
  users_that_caution_on_first_attempt_but_caution_the_second_fac_clear |> Enum.count()

total_users_that_caution_on_first_attempt_but_caution_the_second_fac_consider =
  users_that_caution_on_first_attempt_but_caution_the_second_fac_consider |> Enum.count()

IO.puts(
  "caution first consider second: #{
    total_users_that_caution_on_first_attempt_but_caution_the_second_fac_clear +
      total_users_that_caution_on_first_attempt_but_caution_the_second_fac_consider
  }"
)

fail_map_caution_on_first_attempt_but_caution_the_second_fac_clear =
  users_that_caution_on_first_attempt_but_caution_the_second_fac_clear
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_consider_count_per_result_attempt_num(1)

IO.puts("fail map caution first, caution second fac clear")
IO.inspect(fail_map_caution_on_first_attempt_but_caution_the_second_fac_clear)

fail_map_caution_on_first_attempt_but_caution_the_second_fac_consider =
  users_that_caution_on_first_attempt_but_caution_the_second_fac_consider
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_consider_count_per_result_attempt_num(1)

IO.puts("fail map caution first, caution second fac consider")
IO.inspect(fail_map_caution_on_first_attempt_but_caution_the_second_fac_consider)

users_that_caution_on_first_attempt_fac_consider_but_caution_the_second_fac_clear =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :consider)
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_users_where_attempt_result(2, :caution, :clear)

users_that_caution_on_first_attempt_fac_consider_but_caution_the_second_fac_consider =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :consider)
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_users_where_attempt_result(2, :caution, :consider)

fail_map_caution_on_first_attempt_fac_consider_but_caution_the_second_fac_clear =
  users_that_caution_on_first_attempt_fac_consider_but_caution_the_second_fac_clear
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_consider_count_per_result_attempt_num(1)

IO.puts("fail map caution first fac consider, caution second fac clear")
IO.inspect(fail_map_caution_on_first_attempt_fac_consider_but_caution_the_second_fac_clear)

fail_map_caution_on_first_attempt_fac_consider_but_caution_the_second_fac_consider =
  users_that_caution_on_first_attempt_fac_consider_but_caution_the_second_fac_consider
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_consider_count_per_result_attempt_num(1)

IO.puts("fail map caution first fac consider, caution second fac consider")
IO.inspect(fail_map_caution_on_first_attempt_fac_consider_but_caution_the_second_fac_consider)

users_that_caution_on_first_attempt_but_rejected_the_second_fac_clear =
  users
  |> Get.get_users_where_attempt_result(1, :caution, :clear)
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_users_where_attempt_result(2, :rejected, :clear)

fail_map_caution_on_first_attempt_but_rejected_the_second_fac_clear =
  users_that_caution_on_first_attempt_but_rejected_the_second_fac_clear
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_consider_count_per_result_attempt_num(1)

IO.puts("fail map caution first, rejected second fac consider")
IO.inspect(fail_map_caution_on_first_attempt_but_rejected_the_second_fac_clear)

total_users_that_rejected_on_first_attempt_with_two_tries =
  users
  |> Get.get_users_where_attempt_result(1, :rejected, :clear)
  |> Get.get_where(:num_attempts, 2)
  |> Enum.count()

IO.puts("rejected first two tries: #{total_users_that_rejected_on_first_attempt_with_two_tries}")

users_that_rejected_on_first_attempt_but_clear_the_second =
  users
  |> Get.get_users_where_attempt_result(1, :rejected, :clear)
  |> Get.get_where(:overall_result, :clear)

total_users_that_rejected_on_first_attempt_but_clear_the_second =
  users_that_rejected_on_first_attempt_but_clear_the_second |> Enum.count()

IO.puts(
  "rejected first clear second: #{total_users_that_rejected_on_first_attempt_but_clear_the_second}"
)

users_that_rejected_on_first_attempt_but_rejected_the_second_fac_clear =
  users
  |> Get.get_users_where_attempt_result(1, :rejected, :clear)
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_users_where_attempt_result(2, :rejected, :clear)

users_that_rejected_on_first_attempt_but_rejected_the_second_fac_consider =
  users
  |> Get.get_users_where_attempt_result(1, :rejected, :clear)
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_users_where_attempt_result(2, :rejected, :consider)

fail_map_rejected_on_first_attempt_but_rejected_the_second_fac_clear =
  users_that_rejected_on_first_attempt_but_rejected_the_second_fac_clear
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_consider_count_per_result_attempt_num(1)

IO.puts("fail map rejected first, rejected second fac clear")
IO.inspect(fail_map_rejected_on_first_attempt_but_rejected_the_second_fac_clear)

fail_map_rejected_on_first_attempt_but_rejected_the_second_fac_consider =
  users_that_rejected_on_first_attempt_but_rejected_the_second_fac_consider
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_consider_count_per_result_attempt_num(1)

IO.puts("fail map rejected first, rejected second fac consider")
IO.inspect(fail_map_rejected_on_first_attempt_but_rejected_the_second_fac_consider)

users_that_rejected_on_first_attempt_but_caution_the_second_fac_clear =
  users
  |> Get.get_users_where_attempt_result(1, :rejected, :clear)
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_users_where_attempt_result(2, :caution, :clear)

fail_map_rejected_on_first_attempt_but_caution_the_second_fac_clear =
  users_that_rejected_on_first_attempt_but_caution_the_second_fac_clear
  |> Get.get_where(:num_attempts, 2)
  |> Get.get_consider_count_per_result_attempt_num(1)

IO.puts("fail map rejected first, caution second fac consider")
IO.inspect(fail_map_rejected_on_first_attempt_but_caution_the_second_fac_clear)

properties_count_for_users_doc_caution_fac_clear_first_try =
  users_doc_caution_fac_clear_first_try
  |> Get.get_property_count_per_result_attempt_num(0)

IO.puts("prop count map caution first, fac clear")
IO.inspect(properties_count_for_users_doc_caution_fac_clear_first_try)

properties_count_for_users_that_cleared_on_first_attempt =
  users_that_cleared_on_first_attempt
  |> Get.get_property_count_per_result_attempt_num(0)

IO.puts("prop count map caution first, fac clear")
IO.inspect(properties_count_for_users_that_cleared_on_first_attempt)

properties_count_for_users_doc_rejected_fac_clear_first_try =
  users_doc_rejected_fac_clear_first_try
  |> Get.get_property_count_per_result_attempt_num(0)

IO.puts("prop count map rejected first, fac clear")
IO.inspect(properties_count_for_users_doc_rejected_fac_clear_first_try)

# users_that_caution_on_first_attempt_but_clear_the_second |> Get.get_consider_count_per_result_attempt_num(0)
