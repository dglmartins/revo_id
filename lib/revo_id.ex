defmodule RevoId do
  use Application

  def start(_type, _args) do
    IO.puts("Starting the application...")
    RevoId.Supervisor.start_link()
  end
end
