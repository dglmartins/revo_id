defmodule RevoId.AttemptsParser do
  alias RevoId.Attempt

  alias RevoId.AttemptsParserHelpers, as: Helpers

  @doc """
  Concurrently calls report_file_to_map on both files, parsing in separate processes. Async returns a parsed map combining merged id_attempts of both files. Params is a list of two tuples as `[{path_doc_file, doc_report_code}, {path_fac_file, fac_report_code}]`
  """
  def both_files_async_into_map(params) do
    IO.puts("...please wait...")

    params
    |> Enum.map(&Task.async(fn -> report_file_to_map(&1) end))
    |> Enum.map(&Task.await(&1, :timer.minutes(1)))
    |> merge_doc_fac_maps()
  end

  @doc """
  merges id_attempts map created from doc_reports file with attempts maps created from facial_sim file. Tested in tests
  """
  def merge_doc_fac_maps([attempts_map_from_doc, attempts_map_from_fac]) do
    Map.merge(attempts_map_from_doc, attempts_map_from_fac, fn _k, doc_map, fac_map ->
      Map.merge(doc_map, fac_map, fn key, v_doc, v_fac ->
        merge_values_doc_fac(key, v_doc, v_fac, doc_map, fac_map)
      end)
    end)
  end

  @doc """
  pattern matches attempt_values for deep merge
  """
  # or key == :doc_rep_sub_result
  def merge_values_doc_fac(key, v_doc, v_fac, _doc_map, _fac_map)
      when key == :properties do
    Map.merge(v_doc, v_fac, fn map_key, v_prop_doc, v_prop_fac ->
      merge_values_doc_fac(map_key, v_prop_doc, v_prop_fac, v_doc, v_fac)
    end)
  end

  def merge_values_doc_fac(:overall_result, _v_doc, _v_fac, doc_map, fac_map) do
    Helpers.get_attempt_result(doc_map, fac_map)
  end

  def merge_values_doc_fac(_key, nil, v_fac, _doc_map, _fac_map), do: v_fac
  def merge_values_doc_fac(_key, v_doc, nil, _doc_map, _fac_map), do: v_doc
  def merge_values_doc_fac(_key, v_doc, v_fac, _doc_map, _fac_map), do: v_fac

  @doc """
  Takes the path and code of file and returns a parsed Elixir map. Uses Elixir Flow in functions to concurrently process different lines of the file. Tested in tests.
  """
  def report_file_to_map(path_code_tuple) do
    path_code_tuple
    |> parse_file_into_headers_atoms()
    |> flow_parsed_map()
  end

  @doc """
  Takes the path and code of file and returns a tuple with the path and header titles as Atoms.
  """
  def parse_file_into_headers_atoms({path, rep_code}) do
    headers_atoms =
      File.stream!(path)
      |> Enum.take(1)
      |> List.first()
      |> Helpers.process_line_string()
      |> List.first()
      |> Enum.map(&Helpers.header_title_to_atom(&1, rep_code))

    {path, headers_atoms}
  end

  @doc """
  Takes a tuple of path headers_atoms. Uses Elixir Flow to concurrently process each item of the attempts_list into a map where headers keys point to values. Returns a map. Tested in tests.
  """
  def flow_parsed_map({path, headers_atoms}) do
    File.stream!(path)
    |> Flow.from_enumerable()
    |> Flow.flat_map(&Helpers.process_data_string/1)
    |> Flow.map(fn attempt_data ->
      to_parsed_map(headers_atoms, attempt_data)
    end)
    |> Enum.reduce(%{}, fn parsed_attempt, acc ->
      id_atom =
        ("attempt_" <> parsed_attempt.attempt_id)
        |> String.to_atom()

      Map.put(acc, id_atom, parsed_attempt)
    end)
  end

  @doc """
  Takes headers as atoms and attempt data of same length and returns a parsed Attempt struct, with date, incorrect_ids and properties parsed. Tested in tests.
  """
  def to_parsed_map(headers_atoms, attempt_data) do
    map =
      headers_atoms
      |> Enum.zip(attempt_data)
      |> Enum.into(%{})
      |> Helpers.doc_rep_sub_results_to_atom()
      |> Helpers.code_results()

    struct(Attempt, map)
    |> Helpers.replace_date_string()
    |> Helpers.replace_incorrect_attempt_id()
    |> Helpers.parse_properties_into_struct()
  end
end
