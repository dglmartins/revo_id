defmodule RevoId.AttemptsParserHelpers do
  alias RevoId.Properties

  @incorrect_attempt_id_fac_rep "96948966045741509980950989095520"

  @incorrect_attempt_id_doc_rep "9.6949E+31"

  @updated_attempt_id "abcdefghijklmnopqrstuvwxyz012345"

  @clear :clear

  @consider :consider
  @unidentified :unidentified

  @doc """
  Cleans and splits a string by commas escaping anything between `{}`, ignores first element. Returns a list of the split string.

  ## Examples

      iex> RevoId.AttemptsParserHelpers.process_line_string(~s(this,is,a,test,with,\"{prop1: 'value1', prop2: value2}"\,props))
      [["is", "a", "test", "with", "{prop1: 'value1', prop2: value2}", "props"]]

  """

  def process_line_string(line_string) do
    processed_line =
      line_string
      |> clean_line_string()
      |> split_commas_ignore_first()

    # |> parse_empty_values()

    [processed_line]
  end

  @doc """
  Cleans and splits a string by commas escaping anything between `{}`, ignores first element. Returns an empty list if the string is detected as a header. Returns a list of the split string if is not header by calling process_line_string.

  ## Examples

      iex> RevoId.AttemptsParserHelpers.process_data_string(~s(this,is,a,test,with,\"{prop1: 'value1', prop2: value2}"\,props))
      [["is", "a", "test", "with", "{prop1: 'value1', prop2: value2}", "props"]]
      iex> RevoId.AttemptsParserHelpers.process_data_string(~s(user_id,attempt_id,created_at))
      []

  """
  def process_data_string(line_string) do
    is_header = String.contains?(line_string, "user_id")
    is_empty = line_string == ""

    case is_header or is_empty do
      true ->
        []

      false ->
        process_line_string(line_string)
    end
  end

  defp clean_line_string(line_string) do
    line_string
    |> String.replace("\"", "")
    |> String.trim_trailing("\n")
  end

  defp split_commas_ignore_first(string) do
    [_first | split_string] = Regex.split(~r/,(?=(?:[^{]*{[^}]*})*[^}]*$)/, string)

    split_string
  end

  # defp parse_empty_values(string_list) do
  #   string_list
  #   |> Enum.map(&replace_empty_string/1)
  # end
  #
  # defp replace_empty_string(""), do: @clear
  # defp replace_empty_string(string), do: string

  @doc """
  Converts a string header into an atom after prepending the report_code to fields that are specific to a file.

  ## Examples

      iex> RevoId.AttemptsParserHelpers.header_title_to_atom("colour_picture_result", "doc_rep_")
      :doc_rep_colour_picture_result
      iex> RevoId.AttemptsParserHelpers.header_title_to_atom("user_id", "doc_dep_")
      :user_id

  """
  def header_title_to_atom(header_title, _report_code)
      when header_title == "user_id" or header_title == "attempt_id" or
             header_title == "created_at" or header_title == "properties" do
    String.to_atom(header_title)
  end

  def header_title_to_atom(header_title, report_code) do
    (report_code <> header_title)
    |> String.to_atom()
  end

  @doc """
  Takes an attempt map, replaces the string value of `YYYY-MM-DDTHH:MM:SSZ` at the key :created_at with an elixir naive `DateTime`.

  ## Examples

      iex> map = %{user_id: "abc", created_at: "2017-06-20T18:30:27Z"}
      iex> RevoId.AttemptsParserHelpers.replace_date_string(map)
      %{user_id: "abc", created_at: ~N[2017-06-20 18:30:27]}

  """
  def replace_date_string(%{created_at: date_string} = attempt_map) do
    {:ok, dt, _} = DateTime.from_iso8601(date_string)
    %{attempt_map | created_at: DateTime.to_naive(dt)}
  end

  @doc """
  Fixes incorrect attempt_id in record with pattern match
  """
  def replace_incorrect_attempt_id(%{attempt_id: @incorrect_attempt_id_doc_rep} = attempt_map) do
    %{attempt_map | attempt_id: @updated_attempt_id}
  end

  def replace_incorrect_attempt_id(%{attempt_id: @incorrect_attempt_id_fac_rep} = attempt_map) do
    %{attempt_map | attempt_id: @updated_attempt_id}
  end

  def replace_incorrect_attempt_id(attempt_map) do
    attempt_map
  end

  @doc """
  Guard clause for empty properties
  """
  def parse_properties_into_struct(%{properties: "{}"} = attempt_map) do
    %{attempt_map | properties: %Properties{}}
  end

  @doc """
  Takes an attempt map, replaces the string value of `properties` at the key :properties with an elixir map.

  ## Examples

      iex> map = %{user_id: "abc", properties: "{'gender': 'Male', 'nationality': 'IRL', 'document_type': 'passport', 'date_of_expiry': '2019-08-12', 'issuing_country': 'IRL'}"}
      iex> RevoId.AttemptsParserHelpers.parse_properties_into_struct(map)
      %{
        user_id: "abc",
        properties: %RevoId.Properties{
          date_of_expiry: "2019-08-12",
          document_type: "passport",
          gender: "Male",
          issuing_country: "IRL",
          nationality: "IRL"
        }
      }

  """

  def parse_properties_into_struct(%{properties: string_with_curly_braces} = attempt_map) do
    updated_properties_map =
      string_with_curly_braces
      |> String.replace("{", "")
      |> String.replace("}", "")
      |> String.split(",")
      |> Enum.reduce(%{}, fn kv_string, accum_map ->
        [key, value] = String.split(kv_string, ":")

        key_atom =
          String.replace(key, "'", "")
          |> String.trim()
          |> String.to_atom()

        clean_value =
          String.replace(value, "'", "")
          |> String.trim()

        Map.put(accum_map, key_atom, clean_value)
      end)

    properties_struct = struct(Properties, updated_properties_map)
    %{attempt_map | properties: properties_struct}
  end

  def get_attempt_result(%{doc_rep_result: @clear}, %{fac_rep_result: @clear}), do: @clear

  def get_attempt_result(_doc_map, _fac_map), do: @consider

  def doc_rep_sub_results_to_atom(%{doc_rep_sub_result: sub_result} = attempt_map) do
    sub_result_atom =
      sub_result
      |> String.to_atom()

    %{attempt_map | doc_rep_sub_result: sub_result_atom}
  end

  def doc_rep_sub_results_to_atom(attempt_map), do: attempt_map

  def code_results(attempt_map) do
    attempt_map
    |> Enum.reduce(%{}, fn {key, value}, accum_map ->
      Map.put(accum_map, key, get_results_code(value))
    end)
  end

  defp get_results_code(""), do: @clear
  defp get_results_code("clear"), do: @clear
  defp get_results_code("consider"), do: @consider
  defp get_results_code("unidentified"), do: @unidentified

  defp get_results_code(value), do: value
end
