defmodule RevoId.DataServer do
  @name :data_server

  alias RevoId.GetHelpers, as: Get
  alias RevoId.DataServerInit

  use GenServer

  defmodule State do
    defstruct raw_attempts: %{}, users: %{}, attempts: []
  end

  # Client Interface

  def start_link(_arg) do
    IO.puts("Reading and parsing files into RevoId.DataServer...")
    GenServer.start_link(__MODULE__, %State{}, name: @name)
  end

  def get_state() do
    GenServer.call(@name, :get_state)
  end

  def get_all(key) do
    GenServer.call(@name, {:get_all, key})
  end

  def get_where(state_key, key, value) do
    GenServer.call(@name, {:get_where, state_key, key, value})
  end

  def get_where(state_key, main_key, sub_key, value) do
    GenServer.call(@name, {:get_where, state_key, main_key, sub_key, value})
  end

  # Server Callbacks

  def init(state) do
    DataServerInit.init(state)
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:get_all, key}, _from, state) do
    {:reply, Map.get(state, key), state}
  end

  def handle_call({:get_where, state_key, key, value}, _from, state) do
    results =
      Map.get(state, state_key)
      |> Get.get_where(key, value)

    {:reply, results, state}
  end

  def handle_call({:get_where, state_key, main_key, sub_key, value}, _from, state) do
    results =
      Map.get(state, state_key)
      |> Get.get_where(main_key, sub_key, value)

    {:reply, results, state}
  end

  def handle_info(message, state) do
    IO.puts("Can't touch this! #{inspect(message)}")
    {:noreply, state}
  end
end
