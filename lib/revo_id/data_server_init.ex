defmodule RevoId.DataServerInit do
  alias RevoId.AttemptsParser
  alias RevoId.UsersParser
  alias RevoId.UsersParserHelpers, as: Helpers

  @doc_code "doc_rep_"

  @fac_code "fac_rep_"

  @doc_rep_path Path.expand("../../assets/doc_reports.csv", __DIR__)

  @fac_sim_rep_path Path.expand(
                      "../../assets/facial_similarity_reports.csv",
                      __DIR__
                    )

  def init(state) do
    datetime1 = DateTime.utc_now()
    attempts = fetch_attempts_from_files()
    users = attempts |> parse_users()

    attempts_excl_duplicates_sorted =
      users
      |> get_attempts_excl_duplicates()
      |> Helpers.sort_attempts_by_date()

    datetime2 = DateTime.utc_now()
    time = DateTime.diff(datetime2, datetime1, :seconds)

    IO.puts(
      "Data Parsed and Loaded in #{time} seconds. The Data Server is Running. You can now use server on iex. Type `h RevoId.DataServer. + tab` for help"
    )

    new_state = %{
      state
      | raw_attempts: attempts,
        users: users,
        attempts: attempts_excl_duplicates_sorted
    }

    {:ok, new_state}
  end

  defp fetch_attempts_from_files do
    [{@doc_rep_path, @doc_code}, {@fac_sim_rep_path, @fac_code}]
    |> AttemptsParser.both_files_async_into_map()
  end

  defp parse_users(attempts_map) do
    attempts_map
    |> UsersParser.parse_users()
  end

  def get_attempts_excl_duplicates(users_map) do
    users_map
    |> Enum.reduce([], fn {_k, user}, acc_list ->
      user.attempts ++ acc_list
    end)
  end
end
