defmodule RevoId.GetHelpers do
  @doc """
  gets a value from a state given a key. Pattern matches for different calls with different sub keys and for calls with maps or lists
  """
  def get_where(sub_state, key, value) when is_map(sub_state) do
    sub_state
    |> Enum.filter(fn {_k, map} ->
      Map.get(map, key) == value
    end)
    |> Enum.into(%{})
  end

  def get_where(sub_state, key, value) do
    sub_state
    |> Enum.filter(fn map ->
      Map.get(map, key) == value
    end)
  end

  def get_where(sub_state, main_key, sub_key, value) when is_map(sub_state) do
    sub_state
    |> Enum.filter(fn {_k, map} ->
      sub_map = Map.get(map, main_key)
      Map.get(sub_map, sub_key) == value
    end)
    |> Enum.into(%{})
  end

  def get_where(sub_state, main_key, sub_key, value) do
    sub_state
    |> Enum.filter(fn map ->
      sub_map = Map.get(map, main_key)
      Map.get(sub_map, sub_key) == value
    end)
  end

  @doc """
  gets users based on the results of an attempt. Pattern matches for maps or lists calls.
  """
  def get_users_where_attempt_result(users, attempt_num, attempt_result_doc, attempt_result_fac)
      when is_map(users) do
    users
    |> Enum.filter(fn {_k, map} ->
      doc_rep_results = map.results_sequence.doc_rep_results
      fac_rep_results = map.results_sequence.fac_rep_results

      attempt_index = map.num_attempts - attempt_num

      Enum.at(doc_rep_results, attempt_index) == attempt_result_doc and
        Enum.at(fac_rep_results, attempt_index) == attempt_result_fac
    end)
    |> Enum.into(%{})
  end

  def get_users_where_attempt_result(users, attempt_num, attempt_result_doc, attempt_result_fac) do
    users
    |> Enum.filter(fn map ->
      doc_rep_results = map.results_sequence.doc_rep_results
      fac_rep_results = map.results_sequence.fac_rep_results

      attempt_index = map.num_attempts - attempt_num

      Enum.at(doc_rep_results, attempt_index) == attempt_result_doc and
        Enum.at(fac_rep_results, attempt_index) == attempt_result_fac
    end)
  end

  @doc """
  gets based on not.
  """
  def get_where_not(sub_state, key, value) when is_map(sub_state) do
    sub_state
    |> Enum.filter(fn {_k, map} ->
      Map.get(map, key) != value
    end)
    |> Enum.into(%{})
  end

  def get_where_not(sub_state, key, value) do
    sub_state
    |> Enum.filter(fn map ->
      Map.get(map, key) != value
    end)
  end

  def get_where_not(sub_state, main_key, sub_key, value) when is_map(sub_state) do
    sub_state
    |> Enum.filter(fn {_k, map} ->
      sub_map = Map.get(map, main_key)
      Map.get(sub_map, sub_key) != value
    end)
    |> Enum.into(%{})
  end

  def get_where_not(sub_state, main_key, sub_key, value) do
    sub_state
    |> Enum.filter(fn map ->
      sub_map = Map.get(map, main_key)
      Map.get(sub_map, sub_key) != value
    end)
  end

  def get_attempts_by_doc_rep_sub_result(attempts_map, prop_atom) do
    attempts_map
    |> Enum.filter(fn {_k, v} ->
      sub_results = v.doc_rep_sub_result
      Map.get(sub_results, prop_atom) == true
    end)
  end

  @doc """
  gets fail mode count map given users and attempt index.
  """
  def get_consider_count_per_result_attempt_num(users_map, attempt_index) do
    users_map
    |> Enum.reduce(%{}, fn {_k, user}, accum_map ->
      attempt = Enum.at(user.attempts, attempt_index)
      [_hd | attempt_list] = Map.to_list(attempt)

      attempt_fail_map =
        Enum.reduce(attempt_list, %{}, fn {key, value}, acc ->
          case key do
            :attempt_id -> acc
            :created_at -> acc
            :overall_result -> acc
            :properties -> acc
            :user_id -> acc
            _ -> Map.put(acc, key, check_prop_fail(value))
          end
        end)

      Map.merge(accum_map, attempt_fail_map, fn _k, v_prev, v_curr -> v_prev + v_curr end)
    end)
  end

  def check_prop_fail(:consider), do: 1
  def check_prop_fail(:caution), do: 1
  def check_prop_fail(:rejected), do: 1
  def check_prop_fail(:suspected), do: 1
  def check_prop_fail(:unidentified), do: 1
  def check_prop_fail(_value), do: 0

  @doc """
  gets property count.
  """
  def get_property_count_per_result_attempt_num(users_map, attempt_index) do
    users_map
    |> Enum.reduce(%{}, fn {_k, user}, accum_map ->
      attempt = Enum.at(user.attempts, attempt_index)
      [_hd | property_list] = Map.to_list(attempt.properties)

      property_count_map =
        Enum.reduce(property_list, %{}, fn {key, value}, acc ->
          case key do
            :date_of_expiry -> acc
            :score -> acc
            :gender -> acc
            # :nationality -> acc
            :issuing_country -> acc
            _ -> Map.put(acc, value, 1)
          end
        end)

      Map.merge(accum_map, property_count_map, fn _k, v_prev, v_curr -> v_prev + v_curr end)
    end)
    |> Enum.to_list()
    |> Enum.sort(fn {_, value1}, {_, value2} ->
      value1 >= value2
    end)
  end
end
