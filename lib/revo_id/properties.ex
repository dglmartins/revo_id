defmodule RevoId.Properties do
  defstruct gender: nil,
            nationality: nil,
            document_type: nil,
            date_of_expiry: nil,
            issuing_country: nil,
            score: nil
end
