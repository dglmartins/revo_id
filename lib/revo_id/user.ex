defmodule RevoId.User do
  defstruct attempts: [],
            overall_result: nil,
            user_id: nil,
            num_attempts: 0,
            results_sequence: %{}
end
