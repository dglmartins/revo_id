defmodule RevoId.UsersParser do
  alias RevoId.User
  alias RevoId.UsersParserHelpers, as: Helpers

  @doc """
  Reduces all attempts data to a map of users each as a %User{} struct, that contains the each user's attempt info and overall attempt results.
  """
  def parse_users(attempts_data_map) do
    attempts_data_map
    |> Enum.reduce(%{}, fn {_k, attempt_data}, accum_map ->
      user_id_atom =
        ("user_" <> attempt_data.user_id)
        |> String.to_atom()

      user_curr_attempt = %User{
        user_id: attempt_data.user_id,
        attempts: [attempt_data],
        overall_result: attempt_data.overall_result,
        num_attempts: 1,
        results_sequence: %{
          doc_rep_results: [attempt_data.doc_rep_sub_result],
          fac_rep_results: [attempt_data.fac_rep_result]
        }
      }

      Map.update(accum_map, user_id_atom, user_curr_attempt, &update_user(user_curr_attempt, &1))
    end)
  end

  @doc """
  Updates the user map given a new attempt made by user.
  """
  def update_user(
        %{attempts: [curr_attempt]} = user_curr_attempt,
        %{attempts: prev_attempts} = user_prev_attempt
      ) do
    updated_attempts =
      {curr_attempt, prev_attempts}
      |> Helpers.concat_attempt_ignore_duplicate()
      |> Helpers.sort_attempts_by_date()

    updated_results_sequence =
      updated_attempts
      |> Helpers.update_results_sequence()

    updated_overall_result =
      updated_attempts
      |> List.last()
      |> Map.get(:overall_result)

    updated_num_attempts = Enum.count(updated_attempts)

    %{
      user_prev_attempt
      | attempts: updated_attempts,
        overall_result: updated_overall_result,
        num_attempts: updated_num_attempts,
        results_sequence: updated_results_sequence
    }
  end
end
