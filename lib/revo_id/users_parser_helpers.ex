defmodule RevoId.UsersParserHelpers do
  @doc """
  Concats an attempt, but ignores duplicate user attempts. Duplicate attempts are considered to be the ones that are exactly the same except for the the attempt_id (same time, same results).
  ## Examples

      iex> curr_attempt = %{attempt_id: "a", created_at: ~N[2017-07-27 16:18:20], user_id: "abc", overall_result: "clear"}
      iex> prev_attempts = [%{attempt_id: "b", created_at: ~N[2017-07-27 16:18:15], user_id: "abc", overall_result: "clear"}, %{attempt_id: "c", created_at: ~N[2017-07-27 16:18:20], user_id: "abc", overall_result: "clear"}]
      iex> RevoId.UsersParserHelpers.concat_attempt_ignore_duplicate({curr_attempt, prev_attempts})
      [%{attempt_id: "b", created_at: ~N[2017-07-27 16:18:15], user_id: "abc", overall_result: "clear"}, %{attempt_id: "c", created_at: ~N[2017-07-27 16:18:20], user_id: "abc", overall_result: "clear"}]
  """

  def concat_attempt_ignore_duplicate({curr_attempt, prev_attempts}) do
    curr_attempt_no_key = Map.drop(curr_attempt, [:attempt_id])

    prev_attempts_no_key = Enum.map(prev_attempts, &Map.drop(&1, [:attempt_id]))

    case Enum.member?(prev_attempts_no_key, curr_attempt_no_key) do
      true -> prev_attempts
      false -> [curr_attempt | prev_attempts]
    end
  end

  @doc """
  Sorts attempts list by date.
  ## Examples

      iex> attempts = [%{attempt_id: "a", created_at: ~N[2017-07-27 16:18:30], user_id: "abc", overall_result: "clear"}, %{attempt_id: "b", created_at: ~N[2017-07-27 16:18:15], user_id: "abc", overall_result: "clear"}, %{attempt_id: "c", created_at: ~N[2017-07-27 16:18:20], user_id: "abc", overall_result: "clear"}]
      iex> RevoId.UsersParserHelpers.sort_attempts_by_date(attempts)
      [%{attempt_id: "b", created_at: ~N[2017-07-27 16:18:15], user_id: "abc", overall_result: "clear"}, %{attempt_id: "c", created_at: ~N[2017-07-27 16:18:20], user_id: "abc", overall_result: "clear"}, %{attempt_id: "a", created_at: ~N[2017-07-27 16:18:30], user_id: "abc", overall_result: "clear"} ]
  """
  def sort_attempts_by_date(attempts) do
    attempts
    |> Enum.sort(fn attempt_one, attempt_two ->
      date1 = attempt_one.created_at
      date2 = attempt_two.created_at
      NaiveDateTime.compare(date1, date2) == :lt or NaiveDateTime.compare(date1, date2) == :eq
    end)
  end

  def update_results_sequence(updated_attempts) do
    updated_attempts
    |> Enum.reduce(%{doc_rep_results: [], fac_rep_results: []}, fn attempt, accum_map ->
      doc_rep_results = [attempt.doc_rep_sub_result | accum_map.doc_rep_results]
      fac_rep_results = [attempt.fac_rep_result | accum_map.fac_rep_results]
      %{doc_rep_results: doc_rep_results, fac_rep_results: fac_rep_results}
    end)

    # %{
    #   doc_rep_results: [current_doc_rep_result | prev_doc_rep_results],
    #   fac_rep_results: [current_fac_rep_result | prev_fac_rep_results]
    # }
  end
end
