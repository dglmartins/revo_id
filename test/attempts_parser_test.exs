defmodule AttemptsParserTest do
  use ExUnit.Case

  doctest RevoId.AttemptsParser

  alias RevoId.AttemptsParser, as: Parser

  @doc_code "doc_rep_"

  @fac_code "fac_rep_"

  @clear :clear
  @consider :consider

  @doc_rep_test_path Path.expand("../assets/doc_reports_test.csv", __DIR__)

  @fac_sim_rep_test_path Path.expand(
                           "../assets/facial_similarity_reports_test.csv",
                           __DIR__
                         )

  test "report_file_to_map returns a map of attempts, with attempt_ids as keys, prepended by word attempt" do
    doc_params = {@doc_rep_test_path, @doc_code}
    fac_params = {@fac_sim_rep_test_path, @fac_code}

    map_of_fac_attempts =
      fac_params
      |> Parser.report_file_to_map()

    map_of_doc_attempts =
      doc_params
      |> Parser.report_file_to_map()

    assert Enum.count(map_of_fac_attempts) == 19
    assert Enum.count(map_of_doc_attempts) == 19

    assert map_of_fac_attempts.attempt_050a0596de424fab83c433eaa18b3f8d == %RevoId.Attempt{
             attempt_id: "050a0596de424fab83c433eaa18b3f8d",
             created_at: ~N[2017-06-20 23:12:58],
             fac_rep_face_comparison_result: @clear,
             fac_rep_facial_image_integrity_result: @clear,
             fac_rep_result: @clear,
             fac_rep_visual_authenticity_result: @consider,
             properties: %RevoId.Properties{},
             user_id: "ab23fae164e34af0a1ad1423ce9fd9f0"
           }

    assert map_of_doc_attempts.attempt_050a0596de424fab83c433eaa18b3f8d == %RevoId.Attempt{
             attempt_id: "050a0596de424fab83c433eaa18b3f8d",
             created_at: ~N[2017-06-20 23:12:57],
             doc_rep_colour_picture_result: @clear,
             doc_rep_compromised_document_result: @clear,
             doc_rep_conclusive_document_quality_result: @clear,
             doc_rep_data_comparison_result: @clear,
             doc_rep_data_consistency_result: @clear,
             doc_rep_data_validation_result: @clear,
             doc_rep_face_detection_result: @clear,
             doc_rep_image_integrity_result: @clear,
             doc_rep_image_quality_result: @clear,
             doc_rep_police_record_result: @clear,
             doc_rep_result: @consider,
             doc_rep_sub_result: :caution,
             doc_rep_supported_document_result: @clear,
             doc_rep_visual_authenticity_result: @consider,
             properties: %RevoId.Properties{
               date_of_expiry: "2019-08-12",
               document_type: "passport",
               gender: "Male",
               issuing_country: "IRL",
               nationality: "IRL"
             },
             user_id: "ab23fae164e34af0a1ad1423ce9fd9f0"
           }
  end

  test "merge_doc_fac_maps returns a merged map of doc and fac attempts with the same ids, with attempt_ids as keys, prepended by word attempt" do
    doc_params = {@doc_rep_test_path, @doc_code}
    fac_params = {@fac_sim_rep_test_path, @fac_code}

    map_of_fac_attempts =
      fac_params
      |> Parser.report_file_to_map()

    map_of_doc_attempts =
      doc_params
      |> Parser.report_file_to_map()

    combined_map = Parser.merge_doc_fac_maps([map_of_doc_attempts, map_of_fac_attempts])

    assert Enum.count(combined_map) == 19

    assert combined_map.attempt_050a0596de424fab83c433eaa18b3f8d == %RevoId.Attempt{
             attempt_id: "050a0596de424fab83c433eaa18b3f8d",
             created_at: ~N[2017-06-20 23:12:58],
             doc_rep_colour_picture_result: @clear,
             doc_rep_compromised_document_result: @clear,
             doc_rep_conclusive_document_quality_result: @clear,
             doc_rep_data_comparison_result: @clear,
             doc_rep_data_consistency_result: @clear,
             doc_rep_data_validation_result: @clear,
             doc_rep_face_detection_result: @clear,
             doc_rep_image_integrity_result: @clear,
             doc_rep_image_quality_result: @clear,
             doc_rep_police_record_result: @clear,
             doc_rep_result: @consider,
             doc_rep_sub_result: :caution,
             doc_rep_supported_document_result: @clear,
             doc_rep_visual_authenticity_result: @consider,
             fac_rep_face_comparison_result: @clear,
             fac_rep_facial_image_integrity_result: @clear,
             fac_rep_result: @clear,
             fac_rep_visual_authenticity_result: @consider,
             properties: %RevoId.Properties{
               date_of_expiry: "2019-08-12",
               document_type: "passport",
               gender: "Male",
               issuing_country: "IRL",
               nationality: "IRL"
             },
             user_id: "ab23fae164e34af0a1ad1423ce9fd9f0",
             overall_result: @consider
           }
  end

  test "it parses both test files into a single parsed map" do
    params = [{@doc_rep_test_path, @doc_code}, {@fac_sim_rep_test_path, @fac_code}]

    parsed_map = Parser.both_files_async_into_map(params)

    assert Enum.count(parsed_map) == 19

    assert parsed_map.attempt_050a0596de424fab83c433eaa18b3f8d == %RevoId.Attempt{
             :user_id => "ab23fae164e34af0a1ad1423ce9fd9f0",
             :doc_rep_result => @consider,
             :doc_rep_visual_authenticity_result => @consider,
             :doc_rep_image_integrity_result => @clear,
             :doc_rep_face_detection_result => @clear,
             :doc_rep_image_quality_result => @clear,
             :doc_rep_supported_document_result => @clear,
             :doc_rep_conclusive_document_quality_result => @clear,
             :doc_rep_colour_picture_result => @clear,
             :doc_rep_data_validation_result => @clear,
             :doc_rep_data_consistency_result => @clear,
             :doc_rep_data_comparison_result => @clear,
             :attempt_id => "050a0596de424fab83c433eaa18b3f8d",
             :doc_rep_police_record_result => @clear,
             :doc_rep_compromised_document_result => @clear,
             :properties => %RevoId.Properties{
               gender: "Male",
               nationality: "IRL",
               document_type: "passport",
               date_of_expiry: "2019-08-12",
               issuing_country: "IRL"
             },
             :doc_rep_sub_result => :caution,
             :fac_rep_result => @clear,
             :fac_rep_face_comparison_result => @clear,
             :created_at => ~N[2017-06-20 23:12:58],
             :fac_rep_facial_image_integrity_result => @clear,
             :fac_rep_visual_authenticity_result => @consider,
             :overall_result => @consider
           }
  end
end
