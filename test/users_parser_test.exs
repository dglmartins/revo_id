defmodule UsersParserTest do
  use ExUnit.Case

  doctest RevoId.UsersParser

  @clear :clear
  @consider :consider

  alias RevoId.AttemptsParser, as: AttemptsParser
  alias RevoId.UsersParser, as: UsersParser

  @doc_code "doc_rep_"

  @fac_code "fac_rep_"

  @doc_rep_test_path Path.expand("../assets/doc_reports_test.csv", __DIR__)

  @fac_sim_rep_test_path Path.expand(
                           "../assets/facial_similarity_reports_test.csv",
                           __DIR__
                         )

  test "it parses both test files into a single parsed attempt map then into a users map" do
    params = [{@doc_rep_test_path, @doc_code}, {@fac_sim_rep_test_path, @fac_code}]

    users_map =
      params
      |> AttemptsParser.both_files_async_into_map()
      |> UsersParser.parse_users()

    assert Enum.count(users_map) == 18

    assert users_map.user_b20147d91fdf4ac289ddd80c4be9716a ==
             %RevoId.User{
               attempts: [
                 %RevoId.Attempt{
                   attempt_id: "2db4bd5ed28948af858881e5e261bc2d",
                   created_at: ~N[2017-06-20 19:00:27],
                   doc_rep_colour_picture_result: @clear,
                   doc_rep_compromised_document_result: @clear,
                   doc_rep_conclusive_document_quality_result: @clear,
                   doc_rep_data_comparison_result: @clear,
                   doc_rep_data_consistency_result: @clear,
                   doc_rep_data_validation_result: @clear,
                   doc_rep_face_detection_result: @clear,
                   doc_rep_image_integrity_result: @clear,
                   doc_rep_image_quality_result: @clear,
                   doc_rep_police_record_result: @clear,
                   doc_rep_result: @clear,
                   doc_rep_sub_result: @clear,
                   doc_rep_supported_document_result: @clear,
                   doc_rep_visual_authenticity_result: @clear,
                   fac_rep_face_comparison_result: @clear,
                   fac_rep_facial_image_integrity_result: @clear,
                   fac_rep_result: @clear,
                   fac_rep_visual_authenticity_result: @clear,
                   properties: %RevoId.Properties{
                     date_of_expiry: "2018-01-21",
                     document_type: "passport",
                     gender: "Female",
                     issuing_country: "GBR",
                     nationality: "GBR"
                   },
                   user_id: "b20147d91fdf4ac289ddd80c4be9716a",
                   overall_result: @clear
                 }
               ],
               num_attempts: 1,
               overall_result: @clear,
               user_id: "b20147d91fdf4ac289ddd80c4be9716a",
               results_sequence: %{
                 doc_rep_results: [:clear],
                 fac_rep_results: [:clear]
               }
             }
  end
end
